package com.laymanlearn.swaggerexample.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/sample")
public class ApiSampleController {

    @GetMapping(value = "")
    public String getApiName(){
        return "Return sample get API";
    }

    @PostMapping(value = "")
    public String getInputData(@RequestBody String body){
        return body;
    }
}
