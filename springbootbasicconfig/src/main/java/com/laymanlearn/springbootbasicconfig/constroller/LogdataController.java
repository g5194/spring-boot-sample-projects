package com.laymanlearn.springbootbasicconfig.constroller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "logs")
public class LogdataController {

    @GetMapping("/add")
    public String addLogs(){

        return "Logs added";
    }

}
