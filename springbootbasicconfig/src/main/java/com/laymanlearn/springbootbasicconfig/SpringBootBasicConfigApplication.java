package com.laymanlearn.springbootbasicconfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootBasicConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootBasicConfigApplication.class, args);
	}

}
