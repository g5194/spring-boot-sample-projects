package com.laymanlearn.springboot.service;

import com.laymanlearn.springboot.domain.Book;
import com.laymanlearn.springboot.repository.BookSearchRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Damodar Naidu M
 * @Year: 2022
 */
@Slf4j
@Service
public class BookSearchServiceImpl implements BookSearchService {
    private final static String DEFAULT_SEARCH_FEILDS[] = new String[]{"name", "description", "author"};
    @Autowired
    private BookSearchRepository searchRepository;

    // Find results from Elasticsearch index of "books" by given query
    @Override
    public List<Book> searchBooks(String query) {
        log.info("Init book search by {}", query);
        return searchRepository.findAllByNameOrDescription(query);
    }

    @Override
    public Book addBook(Book book) {
        log.info("Adding book to index");
        book = searchRepository.save(book);
        log.info("Added book to the ES Index with id of {}", book.getId());
        return book;
    }
}
