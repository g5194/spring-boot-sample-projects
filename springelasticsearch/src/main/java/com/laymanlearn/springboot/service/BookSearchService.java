package com.laymanlearn.springboot.service;

import com.laymanlearn.springboot.domain.Book;

import java.util.List;

public interface BookSearchService {
    List<Book> searchBooks(String query);
    Book addBook(Book book);
}
