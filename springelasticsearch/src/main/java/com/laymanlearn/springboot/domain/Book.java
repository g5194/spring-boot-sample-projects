/**
 * Author: Damodar Naidu
 * Year: 2022
 * Purpose: Book class is being used for Entity and DTO
 */
package com.laymanlearn.springboot.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;


@Data
@Document(indexName = "book")
public class Book {
    @Id
    private String id;
    private String name;
    private String description;
    private List<String> authors;
    private int yearOfPublish;
    private int price;
}
