/**
 * Author: Damodar Naidu
 * Year: 2022
 */

package com.laymanlearn.springboot.controller;

import com.laymanlearn.springboot.service.BookSearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.laymanlearn.springboot.domain.Book;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/book")
public class BookSearchController {

    @Autowired
    private BookSearchService searchService;

    /**
     * @param query : Query String
     * @return : List of Books
     */
    @GetMapping(value = "/search")
    public List<Book> searchBooks(@RequestParam("query") String query){
        log.trace("Init controller with query {}", query);
        return searchService.searchBooks(query);
    }

    @PostMapping(value ="/add")
    public Book addToIndex(@RequestBody Book book){
        log.trace("Init add book to the index");
        return searchService.addBook(book);
    }
}
