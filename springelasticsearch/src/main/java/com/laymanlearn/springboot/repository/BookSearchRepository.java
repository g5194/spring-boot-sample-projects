package com.laymanlearn.springboot.repository;

/**
 * @Author: Damodar Naidu M
 * @Year: 2022
 */
import com.laymanlearn.springboot.domain.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface BookSearchRepository extends ElasticsearchRepository<Book, String> {
    List<Book> findAllByNameOrDescription(String query);
}
