package com.laymanlearn.Spring.Boot.Logback.Config.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController(value = "/data")
public class LogdataController {

    @GetMapping(value = "/add")
    public String addLogs(){
        log.trace("Added info trace");
        log.info("Added info log");
        log.warn("Added info warn");
        log.error("Added info error");
        return "Added logs successfully ....";
    }
}
