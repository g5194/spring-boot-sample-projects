package com.laymanlearn.Spring.Boot.Logback.Config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootLogbackConfigApplication {

	public static void main(String[] args) {
		String s = "   <?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"   <configuration>\n" +
				"         <property name=\"log_root\" value=\"logs/app.log\"/>\n" +
				"         <appender name=\"log_files\" class=\"ch.qos.logback.core.rolling.RollingFileAppender\">\n" +
				"            <file>${log_root}</file>\n" +
				"            <rollingPolicy class=\"ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy\">\n" +
				"               <fileNamePattern>logs/archived/app.%d{yyyy-MM-dd}.%i.log.gz</fileNamePattern>\n" +
				"               <!-- each archived file, size max 10MB -->\n" +
				"               <maxFileSize>10MB</maxFileSize>\n" +
				"               <!-- total size of all archive files, if total size > 20GB, it will delete old archived file -->\n" +
				"               <totalSizeCap>20GB</totalSizeCap>\n" +
				"               <!-- 60 days to keep -->\n" +
				"               <maxHistory>60</maxHistory>\n" +
				"            </rollingPolicy>\n" +
				"            <encoder>\n" +
				"               <pattern>%d %p %c{1.} [%t] %m%n</pattern>\n" +
				"            </encoder>\n" +
				"         </appender>\n" +
				"   \n" +
				"         <appender name=\"CONSOLE\" class=\"ch.qos.logback.core.ConsoleAppender\">\n" +
				"            <layout class=\"ch.qos.logback.classic.PatternLayout\">\n" +
				"               <Pattern>\n" +
				"                     %d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n\n" +
				"               </Pattern>\n" +
				"            </layout>\n" +
				"         </appender>\n" +
				"   \n" +
				"         <appender name=\"json\" class=\"ch.qos.logback.core.ConsoleAppender\">\n" +
				"            <layout class=\"ch.qos.logback.contrib.json.classic.JsonLayout\">\n" +
				"               <jsonFormatter\n" +
				"                        class=\"ch.qos.logback.contrib.jackson.JacksonJsonFormatter\">\n" +
				"                     <prettyPrint>true</prettyPrint>\n" +
				"               </jsonFormatter>\n" +
				"               <timestampFormat>yyyy-MM-dd' 'HH:mm:ss.SSS</timestampFormat>\n" +
				"            </layout>\n" +
				"         </appender>\n" +
				"   \n" +
				"         <logger name=\"com.laymanlearn.Spring.Boot.Logback.Config\" level=\"debug\" additivity=\"false\">\n" +
				"            <appender-ref ref=\"log_files\"/>\n" +
				"            <appender-ref ref=\"CONSOLE\"/>\n" +
				"            <appender-ref ref=\"json\"/>\n" +
				"         </logger>\n" +
				"         <root level=\"info\">\n" +
				"            <appender-ref ref=\"log_files\"/>\n" +
				"         </root>\n" +
				"   </configuration>";
		SpringApplication.run(SpringBootLogbackConfigApplication.class, args);
	}

}
