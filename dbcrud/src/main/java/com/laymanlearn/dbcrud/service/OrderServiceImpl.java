package com.laymanlearn.dbcrud.service;

import com.laymanlearn.dbcrud.domain.Order;
import com.laymanlearn.dbcrud.domain.OrderDto;
import com.laymanlearn.dbcrud.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public OrderDto get(Long id) {
        Order order = orderRepository.findById(id).orElse(null);
        return OrderDto.toDto(order);
    }

    @Override
    public List<OrderDto> get() {
        List<Order> orders = orderRepository.findAll();
        return OrderDto.toDtos(orders);
    }

    @Override
    public String create(OrderDto orderDto) {
        orderRepository.save(orderDto.toEntity());
        return "Successfully.. created";
    }

    @Override
    public OrderDto update(OrderDto orderDto) {
        Order order = orderRepository.save(orderDto.toEntity());
        return OrderDto.toDto(order);
    }

    @Override
    public String delete(Long id) {
        orderRepository.deleteById(id);
        return "Record deleted successfully...";
    }
}
