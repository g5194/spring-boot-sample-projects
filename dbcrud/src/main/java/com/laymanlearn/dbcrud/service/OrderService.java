package com.laymanlearn.dbcrud.service;

import com.laymanlearn.dbcrud.domain.OrderDto;

import java.util.List;

public interface OrderService {
    OrderDto get(Long id);
    List<OrderDto> get();
    String create(OrderDto orderDto);
    OrderDto update(OrderDto orderDto);
    String delete(Long id);
}
