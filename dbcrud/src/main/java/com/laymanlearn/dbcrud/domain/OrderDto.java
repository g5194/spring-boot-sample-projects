package com.laymanlearn.dbcrud.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class OrderDto {
    private Long id;
    private List<Long> orderItems;
    private Long orderByUserId;
    private Long orderFromUserId;
    private double orderValue;

    public Order toEntity(){
        Order order = new Order();
        BeanUtils.copyProperties(this, order);
        return order;
    }

    public static OrderDto toDto(Order order){
        if(order == null){
            return null;
        }
        OrderDto orderDto = new OrderDto();
        BeanUtils.copyProperties(order, orderDto);
        return orderDto;
    }

    public static List<OrderDto> toDtos(List<Order> orders){
        return orders.stream().map(OrderDto::toDto).collect(Collectors.toList());
    }
}
