package com.laymanlearn.dbcrud.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ORDER")
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    private List<Long> orderItems;
    private Long orderByUserId;
    private Long orderFromUserId;
    private double orderValue;
}
