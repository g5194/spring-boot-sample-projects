package com.laymanlearn.dbcrud.controller;

import com.laymanlearn.dbcrud.domain.OrderDto;
import com.laymanlearn.dbcrud.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping(value = "/${orderId}")
    public OrderDto get(@PathVariable("orderId") Long id){
        return orderService.get(id);
    }

    @GetMapping(value = "/")
    public List<OrderDto> get(){
        return orderService.get();
    }

    @PostMapping(value = "/")
    public String create(@RequestBody OrderDto orderDto){
        return orderService.create(orderDto);
    }

    @PutMapping(value = "/")
    public OrderDto update(@RequestBody OrderDto orderDto){
        return orderService.update(orderDto);
    }

    @DeleteMapping(value = "/${orderId}")
    public String delete(@PathVariable("orderId") Long id){
        return orderService.delete(id);
    }
}
