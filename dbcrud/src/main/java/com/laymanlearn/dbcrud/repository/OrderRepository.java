package com.laymanlearn.dbcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.laymanlearn.dbcrud.domain.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
