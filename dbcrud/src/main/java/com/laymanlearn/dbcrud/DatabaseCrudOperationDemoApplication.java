package com.laymanlearn.dbcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseCrudOperationDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseCrudOperationDemoApplication.class, args);
	}

}
