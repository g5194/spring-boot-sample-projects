package com.laymanlearn.springes.service;

import com.laymanlearn.springes.domain.Book;

import java.util.List;

public interface BookSearchService {
    List<Book> searchBooks(String query);
    Book addBook(Book book);
}
