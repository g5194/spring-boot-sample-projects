package com.laymanlearn.interviewprograms.palindrome;

public class PalindromeBothends {
    public static void main(String[] args) {
        String value = "madam";
        System.out.println(value + " is palindrome :" + isPalindrome(value));
    }

    public static boolean isPalindrome(String value) {
        if(value == null || value.length() == 1){
            return false;
        }
        /**
         *  Start comparing from both ends
         *  n: Length
         *  i: Index
         *  i, (n-1)-i
         *  i+1, (n-1)-(i+1)
         *  Stop condition
         *  n/2 >i
         *
         *  Ex: n is 8
         *  8/2 > i
         *  i satisfies till 3 value
         *  Forward i : 0,1,2,3  Backward (n-1)-(i)  7,6,5,4
         *
         *  Ex: n is 7
         *  i satisfies till 2 value
         *  Forward i : 0,1,2 Backward (n-1)-(i) 6,5,4
         *  Here, Index 3 remains odd number need charector need not to verify
         *
         */
        for (int i = 0; (value.length()/2) > i; i--) {
            if(value.charAt(i) != value.charAt((value.length()-1)-i)){
                return false;
            }
        }
        return true;
    }
}

