package com.laymanlearn.interviewprograms.palindrome;

public class PalindromeSimpleReverse {
//    public static void main(String[] args) {
//        String value = "madam";
//        System.out.println(value + " is palindrome :" + isPalindrome(value));
//    }

    public static boolean isPalindrome(String value) {
        StringBuffer reverse = new StringBuffer();
        for (int i = value.length() - 1; i >= 0; i--) {
            reverse.append(value.charAt(i));
        }
        return value.equals(reverse);
    }
}

